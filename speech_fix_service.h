/*---------------------------------------------------------------------------------------------------
 * \file scansoft_speech_fix\speech__fix_service.h
 *
 * \brief Declares the speech fix service class. 
 *-------------------------------------------------------------------------------------------------*/

#ifndef SPEECH_FIX_SERVICE
#define SPEECH_FIX_SERVICE
#include <windows.h>
#include <stdio.h>
#include <Msi.h>
#include <iostream>  
#include <string>  

int WriteToLog(char* str);
void main();
void ServiceMain(int argc, char** argv);
int InitService();
void ControlHandler(DWORD request);
bool CheckSpeechDLLexisting(void);
bool CheckRegistryKey(void);
bool setRegistryKey(void);

// Helpers
std::wstring MsiQueryProperty(LPCTSTR szProductCode, LPCTSTR szUserSid, MSIINSTALLCONTEXT dwContext, LPCTSTR szProperty);
std::wstring RegistryQueryValue(HKEY hKey, LPCTSTR szName);
std::wstring getNuanceFilePath(void);
#endif
