#include "speech_fix_service.h"
#include <system_folders.h>

// #define tcout       wcout  
// #define wstring     wstring  

#define SLEEP_TIME 5000 // 5 second check
#define SLEEP_TIME_NO_ADDON 10*60*1000 // 10 minutes sleep if not found
#define LOGFILE "C:\\usr\\code\\speechfix.txt"
#define CONSOLE_TEST 0
#if CONSOLE_TEST == 1
#define LOG_TO_CONSOLE 1
#else
#define LOG_TO_CONSOLE 0
#endif
#ifdef _DEBUG
#define DO_LOG 1
#else
#define DO_LOG 1
#endif

enum SPEECH_DLL_STATUS {
    SPEECH_DLL_STATUS_UNKNOWN,
    SPEECH_DLL_STATUS_OK,
    SPEECH_DLL_STATUS_NOT_FOUND,
};

SERVICE_STATUS ServiceStatus; 
SERVICE_STATUS_HANDLE hStatus; 

void  ServiceMain(int argc, char** argv); 
void  ControlHandler(DWORD request); 
int   InitService();

#if DO_LOG == 1
int WriteToLog(const char* _Format, ...)
{
#if LOG_TO_CONSOLE == 0
    FILE* log;
    errno_t err;
    err = fopen_s(&log, LOGFILE, "a+");
    if (log == NULL)
        return -1;
    //fprintf(log, "%s\n", str);

    va_list arglist;
    va_start( arglist, _Format );
    vfprintf_s(log,  _Format, arglist );
    va_end(arglist);
    fprintf(log, "\n");

    fclose(log);
#endif
#if LOG_TO_CONSOLE == 1
    va_list arglist;
    va_start( arglist, _Format );
    vprintf_s(_Format, arglist );
    va_end(arglist);
    printf("\n");
#endif
    return 0;
}

#endif

void main() 
{ 
#if CONSOLE_TEST
    err = CheckSpeechDLLexisting();
    WriteToLog("CheckSpeechDLLexisting %d", err);
    err = CheckRegistryKey();
    WriteToLog("CheckRegistryKey %d", err);
    if (err == false) {
       err = setRegistryKey();
       WriteToLog("setRegistryKey %d", err);
    }
#endif

#if CONSOLE_TEST == 0
     SERVICE_TABLE_ENTRY ServiceTable[2];
     ServiceTable[0].lpServiceName = L"ScanSoftSpeechFix";
     ServiceTable[0].lpServiceProc = (LPSERVICE_MAIN_FUNCTION)ServiceMain;
 
     ServiceTable[1].lpServiceName = NULL;
     ServiceTable[1].lpServiceProc = NULL;
     // Start the control dispatcher thread for our service
     StartServiceCtrlDispatcher(ServiceTable);  
#endif
}


void ServiceMain(int argc, char** argv) 
{ 
    int error; 
    int current_sleep_time;
    int verified_dll_existanse = SPEECH_DLL_STATUS_UNKNOWN;
    current_sleep_time = SLEEP_TIME;

    ServiceStatus.dwServiceType        = SERVICE_WIN32; 
    ServiceStatus.dwCurrentState       = SERVICE_START_PENDING; 
    ServiceStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN;
    ServiceStatus.dwWin32ExitCode      = 0; 
    ServiceStatus.dwServiceSpecificExitCode = 0; 
    ServiceStatus.dwCheckPoint         = 0; 
    ServiceStatus.dwWaitHint           = 0; 

    hStatus = RegisterServiceCtrlHandler( L"ScanSoftSpeechFix",  (LPHANDLER_FUNCTION)ControlHandler); 
    if (hStatus == (SERVICE_STATUS_HANDLE)0) 
    { 
        // Registering Control Handler failed
        return; 
    }  
    // Initialize Service 
    error = InitService(); 
    if (error) 
    {
        // Initialization failed
        ServiceStatus.dwCurrentState       = SERVICE_STOPPED; 
        ServiceStatus.dwWin32ExitCode      = -1; 
        SetServiceStatus(hStatus, &ServiceStatus); 
        return; 
    } 
    // We report the running status to SCM. 
    ServiceStatus.dwCurrentState = SERVICE_RUNNING; 
    SetServiceStatus (hStatus, &ServiceStatus);

//    MEMORYSTATUS memory;
    // The worker loop of a service
    while (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
    {
        // check if Symwriter Speech Addon is installed
        // if not, set sleep to 10s (10k ms)
        if (verified_dll_existanse != SPEECH_DLL_STATUS_OK) {
            if (CheckSpeechDLLexisting() == false) {
                verified_dll_existanse = SPEECH_DLL_STATUS_NOT_FOUND;
                current_sleep_time = SLEEP_TIME_NO_ADDON; // 5 minute delay
            }
            else {
                verified_dll_existanse = SPEECH_DLL_STATUS_OK;
            }
        }
        if (verified_dll_existanse == SPEECH_DLL_STATUS_OK) {
            // addon is installed and file is found
            // check the registry key :)
            if (CheckRegistryKey() == false) {
                setRegistryKey();
            }
        }

        Sleep(current_sleep_time);
    }
    return; 
}

// Service initialization
int InitService() 
{ 
#if DO_LOG == 1
    int result;
    result = WriteToLog("Started monitoring registry.");
    return(result); 
  #else
    return 0;
  #endif
} 

// Control handler function
void ControlHandler(DWORD request)  { 
    switch(request) 
    { 
    case SERVICE_CONTROL_STOP: 
#if DO_LOG == 1
        WriteToLog("Stopped monitoring registry.");
#endif

        ServiceStatus.dwWin32ExitCode = 0; 
        ServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
        SetServiceStatus (hStatus, &ServiceStatus);
        return; 

    case SERVICE_CONTROL_SHUTDOWN: 
#if DO_LOG == 1
        WriteToLog("Stopped monitoring registry.");
#endif
        ServiceStatus.dwWin32ExitCode = 0; 
        ServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
        SetServiceStatus (hStatus, &ServiceStatus);
        return; 

    default:
        break;
    } 

    // Report current status
    SetServiceStatus (hStatus,  &ServiceStatus);

    return; 
}

bool CheckRegistryKey(void) {
#if DO_LOG == 1
    WriteToLog("CheckRegistryKey");
#endif
    HKEY hKey;  
    // must check here if we are given 32 bit or 64 bit keys?
    LONG ret = ::RegOpenKeyEx(  
        HKEY_LOCAL_MACHINE,     // local machine hive  
        L"SOFTWARE\\Classes\\CLSID\\{CAC6785B-655E-4AE1-A656-BDEFD18DC46C}\\InprocServer32", // key for scansoft speech stuff
        0,                      // reserved  
        KEY_READ,               // desired access  
        &hKey                   // handle to the open key
        );  

    if(ret != ERROR_SUCCESS)  
        return false;
    std::wstring nuance_file = getNuanceFilePath();
    nuance_file.resize(nuance_file.length() + 1);

    if ( nuance_file.compare(RegistryQueryValue(hKey, L"")) == 0 ) {
#if DO_LOG == 1
        WriteToLog("CheckRegistryKey: Key OK");
#endif
        return true;
    }

#if DO_LOG == 1
    WriteToLog("CheckRegistryKey: Key is wrong");
#endif
    return false;
}

std::wstring RegistryQueryValue(HKEY hKey, LPCTSTR szName)   {  
    std::wstring value;  

    DWORD dwType;  
    DWORD dwSize = 0;  

    if (::RegQueryValueEx(  
        hKey,                   // key handle  
        szName,                 // item name  
        NULL,                   // reserved  
        &dwType,                // type of data stored  
        NULL,                   // no data buffer  
        &dwSize                 // required buffer size  
        ) == ERROR_SUCCESS && dwSize > 0)  
    {  
        value.resize(dwSize/(sizeof(wchar_t)));  

        ::RegQueryValueEx(  
            hKey,                   // key handle  
            szName,                 // item name  
            NULL,                   // reserved  
            &dwType,                // type of data stored  
            (LPBYTE)&value[0],      // data buffer  
            &dwSize                 // available buffer size  
            );  
    }  

    return value;  
}  

bool setRegistryKey(void) {
#if DO_LOG == 1
    WriteToLog("setRegistryKey");
#endif
    HKEY hKey;  
    // must check here if we are given 32 bit or 64 bit keys?
    LONG ret = ::RegOpenKeyEx(  
        HKEY_LOCAL_MACHINE,     // local machine hive  
        L"SOFTWARE\\Classes\\CLSID\\{CAC6785B-655E-4AE1-A656-BDEFD18DC46C}\\InprocServer32", // key for scansoft speech stuff
        0,                      // reserved  
        KEY_WRITE,              // desired access  
        &hKey                   // handle to the open key
        );  

    if (ret != ERROR_SUCCESS) {
#if DO_LOG == 1
        WriteToLog("setRegistryKey: Cannot open key for writing");
#endif
        return false;
    }

    std::wstring nuance_file = getNuanceFilePath();
    if (nuance_file == std::wstring()) {
#if DO_LOG == 1
        WriteToLog("setRegistryKey: nuance_file empty??");
#endif
        return false;
    }

    ret = RegSetValueEx(
        hKey,                   // key handle  
        L"",                    // item name  
        NULL,                   // reserved  
        REG_SZ,                 // type of data to store
        (LPBYTE)& nuance_file[0], // data to write
        (DWORD) ((nuance_file.length() + 1 )* sizeof(wchar_t)) // size of the data to write (in bytes)
        );

    if (ret != ERROR_SUCCESS) {
#if DO_LOG == 1
        WriteToLog("setRegistryKey: Cannot write key");
#endif
        return false;
    }

#if DO_LOG == 1
    WriteToLog("setRegistryKey: Updated key");
#endif

    return true;
}

bool CheckSpeechDLLexisting(void) {
#if DO_LOG == 1
    WriteToLog("CheckSpeechDLLexisting");
#endif
    struct _stat stFileInfo;
    int intStat = 0;

    std::wstring nuance_file = getNuanceFilePath();
    if (nuance_file == std::wstring()) {
#if DO_LOG == 1
        WriteToLog("CheckSpeechDLLexisting: nuance_file empty??");
#endif
        return false;
    }

    intStat = _wstat( nuance_file.c_str() , &stFileInfo);
    if(intStat == 0) {
#if DO_LOG == 1
        WriteToLog("CheckSpeechDLLexisting; FILE FOUND");
#endif
        return true;
    }
#if DO_LOG == 1
    WriteToLog("CheckSpeechDLLexisting; FILE NOT FOUND");
#endif
    return false;
}

std::wstring getNuanceFilePath(void) {
    static wchar_t nuance_file[255];
    if (getFolderPath(FolderType_ProgramFilesX86, nuance_file) < 0 ) {
        return std::wstring();
    }
    // add:
    // Nuance Realspeak Solo 4\speech\components\common\rs_sapi5_solo.dll
    wcscat_s(nuance_file, L"\\Nuance Realspeak Solo 4\\speech\\components\\common\\rs_sapi5_solo.dll\0");
    std::wstring value(nuance_file);
#if DO_LOG == 1
    WriteToLog("  -- getNuanceFilePath: %S", value.c_str());
#endif
    return value;
}

std::wstring MsiQueryProperty(LPCTSTR szProductCode,  
                         LPCTSTR szUserSid,  
                         MSIINSTALLCONTEXT dwContext,  
                         LPCTSTR szProperty)  
{
    wchar_t tmp_wchar_value[500];

    DWORD cchValue = 0;  
    UINT ret2 = ::MsiGetProductInfoEx(  
        szProductCode,  
        szUserSid,  
        dwContext,  
        szProperty,  
        NULL,  
        &cchValue);

    if(ret2 == ERROR_SUCCESS)  
    {  
        cchValue++;  

        ret2 = ::MsiGetProductInfoEx(  
            szProductCode,  
            szUserSid,  
            dwContext,  
            szProperty,  
            /*(LPTSTR)&value[0],  */
            tmp_wchar_value, 
            &cchValue);  
    }  

    std::wstring value(tmp_wchar_value);
    return value;  
}  
